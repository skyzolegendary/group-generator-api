FROM php:8.1-apache

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y --no-install-recommends \
    locales apt-utils libicu-dev g++ libpng-dev libxml2-dev libzip-dev libonig-dev libxslt-dev zip libpq-dev git

# Add translate files
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen && \
    locale-gen

# Composer

RUN curl -sSk https://getcomposer.org/installer | php -- --disable-tls
RUN mv composer.phar /usr/local/bin/composer

# PHP extensions

RUN docker-php-ext-configure intl
RUN docker-php-ext-install pdo intl zip dom mbstring zip xsl gd

RUN pecl install apcu
RUN docker-php-ext-enable apcu

#RUN docker-php-ext-install opcache
#RUN docker-php-ext-enable opcache
RUN docker-php-ext-install mysqli pdo pdo_mysql

# Création vhost
WORKDIR /var/www/html/

ENV PHP_MEMORY_LIMIT=256M
ENV APACHE_DOCUMENT_ROOT=/var/www/html/public

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash
RUN apt-get install symfony-cli
#RUN curl -sS https://get.symfony.com/cli/instaler | bash
#RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony

# Init apache extension
RUN a2enmod rewrite

