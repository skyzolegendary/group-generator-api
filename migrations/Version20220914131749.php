<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220914131749 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE diploma (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `group` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE group_student (id INT AUTO_INCREMENT NOT NULL, global_group_id INT DEFAULT NULL, INDEX IDX_3123FB3F29328F69 (global_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `option` (id INT AUTO_INCREMENT NOT NULL, diploma_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_5A8600B0A99ACEB5 (diploma_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE school_class (id INT AUTO_INCREMENT NOT NULL, diploma_id INT DEFAULT NULL, option_id INT DEFAULT NULL, label VARCHAR(255) NOT NULL, INDEX IDX_33B1AF85A99ACEB5 (diploma_id), INDEX IDX_33B1AF85A7C41D6F (option_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE staff (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_426EF392E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student (id INT AUTO_INCREMENT NOT NULL, school_class_id INT DEFAULT NULL, group_student_id INT DEFAULT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, INDEX IDX_B723AF3314463F54 (school_class_id), INDEX IDX_B723AF331C592EA8 (group_student_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE group_student ADD CONSTRAINT FK_3123FB3F29328F69 FOREIGN KEY (global_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE `option` ADD CONSTRAINT FK_5A8600B0A99ACEB5 FOREIGN KEY (diploma_id) REFERENCES diploma (id)');
        $this->addSql('ALTER TABLE school_class ADD CONSTRAINT FK_33B1AF85A99ACEB5 FOREIGN KEY (diploma_id) REFERENCES diploma (id)');
        $this->addSql('ALTER TABLE school_class ADD CONSTRAINT FK_33B1AF85A7C41D6F FOREIGN KEY (option_id) REFERENCES `option` (id)');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF3314463F54 FOREIGN KEY (school_class_id) REFERENCES school_class (id)');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF331C592EA8 FOREIGN KEY (group_student_id) REFERENCES group_student (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE group_student DROP FOREIGN KEY FK_3123FB3F29328F69');
        $this->addSql('ALTER TABLE `option` DROP FOREIGN KEY FK_5A8600B0A99ACEB5');
        $this->addSql('ALTER TABLE school_class DROP FOREIGN KEY FK_33B1AF85A99ACEB5');
        $this->addSql('ALTER TABLE school_class DROP FOREIGN KEY FK_33B1AF85A7C41D6F');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF3314463F54');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF331C592EA8');
        $this->addSql('DROP TABLE diploma');
        $this->addSql('DROP TABLE `group`');
        $this->addSql('DROP TABLE group_student');
        $this->addSql('DROP TABLE `option`');
        $this->addSql('DROP TABLE school_class');
        $this->addSql('DROP TABLE staff');
        $this->addSql('DROP TABLE student');
    }
}
