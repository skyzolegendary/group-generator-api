<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\GroupStudentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupStudentRepository::class)]
#[ApiResource]
class GroupStudent
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToMany(mappedBy: 'groupStudent', targetEntity: Student::class)]
    private Collection $student;

    #[ORM\ManyToOne(inversedBy: 'groupStudents')]
    private ?Group $globalGroup = null;

    public function __construct()
    {
        $this->student = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Student>
     */
    public function getStudent(): Collection
    {
        return $this->student;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->student->contains($student)) {
            $this->student->add($student);
            $student->setGroupStudent($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->student->removeElement($student)) {
            // set the owning side to null (unless already changed)
            if ($student->getGroupStudent() === $this) {
                $student->setGroupStudent(null);
            }
        }

        return $this;
    }

    public function getGlobalGroup(): ?Group
    {
        return $this->globalGroup;
    }

    public function setGlobalGroup(?Group $globalGroup): self
    {
        $this->globalGroup = $globalGroup;

        return $this;
    }
}
