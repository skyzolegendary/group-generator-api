<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GroupRepository::class)]
#[ORM\Table(name: '`group`')]
#[ApiResource]
class Group
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'globalGroup', targetEntity: GroupStudent::class)]
    private Collection $groupStudents;

    public function __construct()
    {
        $this->groupStudents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, GroupStudent>
     */
    public function getGroupStudents(): Collection
    {
        return $this->groupStudents;
    }

    public function addGroupStudent(GroupStudent $groupStudent): self
    {
        if (!$this->groupStudents->contains($groupStudent)) {
            $this->groupStudents->add($groupStudent);
            $groupStudent->setGlobalGroup($this);
        }

        return $this;
    }

    public function removeGroupStudent(GroupStudent $groupStudent): self
    {
        if ($this->groupStudents->removeElement($groupStudent)) {
            // set the owning side to null (unless already changed)
            if ($groupStudent->getGlobalGroup() === $this) {
                $groupStudent->setGlobalGroup(null);
            }
        }

        return $this;
    }
}
